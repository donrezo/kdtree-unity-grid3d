# KDTreeUnity Grid Server File Exporter

## Overview
The KDTreeUnity project comprises two main components:

*  Unity Grid Editor: A Unity-based grid editor that utilizes a K-D Tree for efficient spatial querying and manipulation. This editor allows users to interactively create and modify a 3D grid, designating walkable and unwalkable areas.

* K-D Tree Benchmark in .NET: A standalone .NET application for benchmarking the performance of the K-D Tree implementation, particularly focusing on insertion and search operations under various conditions.

# KDTree Performance Benchmark

## Benchmark Results

### Environment
- **Runtime**: .NET 8.0.0 on macOS Sonoma 14.0
- **Processor**: Intel Core i7-9750H CPU, 12 logical cores

### Benchmark Summary

| Benchmark               | Mean Time          | Standard Error    | Standard Deviation | Min Time        | Max Time        | Confidence Interval         | Skewness | Kurtosis |
|-------------------------|-------------------:|------------------:|-------------------:|----------------:|----------------:|---------------------------:|---------:|---------:|
| **InsertionBenchmark**  | 11.36 ms           | 0.352 ms          | 3.523 ms           | 5.293 ms        | 17.399 ms       | [10.165 ms; 12.555 ms]      | 0.01     | 1.79     |
| **SearchBenchmark**     | 176.680 ns         | 0.089 ns          | 0.332 ns           | 176.088 ns      | 177.171 ns      | [176.305 ns; 177.055 ns]    | -0.38    | 1.77     |
| **SearchStressTest**    | 19.876 ms          | 0.063 ms          | 0.235 ms           | 19.584 ms       | 20.306 ms       | [19.611 ms; 20.140 ms]      | 0.55     | 1.87     |
| **InsertionStressTest** | 133.946 s          | 0.263 s           | 1.018 s            | 131.777 s       | 135.317 s       | [132.858 s; 135.035 s]      | -0.58    | 2.24     |

## Performance Analysis

- **InsertionBenchmark**: Moderate performance with a mean time of 11.36 ms. High standard deviation indicates variability in insertion times.
- **SearchBenchmark**: Highly efficient with a mean time of 176.680 ns. Consistent performance across different search operations.
- **SearchStressTest**: Moderate performance under stress with a mean time of 19.876 ms. Reliable performance under larger datasets.
- **InsertionStressTest**: Lower performance in large-scale insertions with a mean time of 133.946 seconds. Points to potential areas for improvement in scalability.

## Conclusion

The KdTree demonstrates excellent efficiency in search operations but indicates a need for optimization in handling large-scale insertions.

## Artifacts

- Detailed results and diagnostics are available in CSV, Markdown, and HTML formats.

---

*Note: This benchmark was conducted using BenchmarkDotNet v0.13.10 on macOS Sonoma 14.0 with an Intel Core i7-9750H processor.*

TestFile provided in KDTreeBenchmark project. 
* Map_001.txt


## Unity Grid Editor - PlayMode

In Play mode MapVisualizer show KD structure which you prepared in edit mode. Remember to save output file in Resources/Map/map_XXX.txt
[![EditMode2.png](./PlayMode.png)]()


## Unity Grid Editor

In Edit mode when you add GridData3D on object in hierarchy you can edit grid in scene view. You can change grid size and node size in GridData3D component. You can also save and load grid configurations.
* Clink on Layer button to change layer of grid.
* To paint hold click, its setup this way as for now
[![EditMode.png](./EditMode.png)]()
[![EditMode2.png](./EditMode2.png)]()

### Features
* Interactive 3D grid editing within the Unity Editor.
* Ability to toggle between walkable and unwalkable nodes.
* Functionality to save and load grid configurations.
* Customizable grid dimensions and node sizes.
* Visualization of K-D Tree structure for educational and debugging purposes.
### Key Components
* GridData3D: Manages the 3D grid data, providing methods for setting walkability, saving/loading grids, and updating grid sizes.
* GridEditor3D: A custom Unity editor script for interactive grid editing within the Scene View.
* MapVisualizer: A component that visualizes the K-D Tree structure in the Unity Scene View.
## K-D Tree Benchmark in .NET
### Benchmarking
The .NET application uses BenchmarkDotNet to measure the performance of the K-D Tree in various scenarios:

* InsertionBenchmark: Measures the time taken to insert a new node into the K-D Tree.
* SearchBenchmark: Measures the time taken to search for an existing node in the K-D Tree.
* Stress Tests: Evaluates the performance under high-load conditions, testing both insertion and search operations with a large number of operations.
### Setup and Running Benchmarks
* The benchmarks are located in the KDTreeBenchmark project.
* Use the provided Makefile to build and run benchmarks.
## Make Commands

```make build: Builds both Unity and .NET projects.```

```make test: Runs tests for both Unity and .NET projects.```

``` make benchmark: Runs benchmarks for the .NET project.```

```make unity-build: Builds the Unity project.``` 

```make unity-test: Runs tests in the Unity project.```

```make dotnet-build: Builds the .NET project.```

```make dotnet-test: Runs tests in the .NET project.```

```make dotnet-benchmark: Runs benchmarks in the .NET project.```

## Project Structure
* Grid3D: The directory containing the Unity project.
* KDTreeBenchmark: The directory for the .NET benchmarking project.
## Installation and Setup
* Clone the repository.
* Navigate to the respective project directories for Unity and .NET tasks.
* Use the Makefile for building, testing, and running benchmarks.
## Contributing
Contributions are welcome. Please adhere to the project's coding standards and submit pull requests for any enhancements.

## License
This project is licensed under the MIT License feel free to use it however you wont.

Note: This project was created for educational purposes and is not intended for commercial use. I dont take any responsibility for any damages caused by this project.