namespace KDTreeBenchmark
{
    public class KdTree
{
    public struct Pos3D
    {
        public Pos3D(int x, int y, int z) { X = x; Y = y; Z = z; }
        public int X;
        public int Y;
        public int Z;
    }
    
    public struct PQNode3D : IComparable<PQNode3D>
    {
        public int F; // Total cost (G + H)
        public int G; // Cost from start to node
        public Pos3D Position; // Position in 3D space

        public int CompareTo(PQNode3D other)
        {
            return F.CompareTo(other.F);
        }
    }

    public class Node
    {
        public Pos3D Position;
        public Node Left;
        public Node Right;

        public Node(Pos3D position)
        {
            Position = position;
        }
    }

    private Node root;
    public Node Root => root;
    public void Insert(Pos3D position)
    {
        root = InsertRec(root, position, 0);
    }

    private Node InsertRec(Node node, Pos3D position, int depth)
    {
        if (node == null)
            return new Node(position);

        int cd = depth % 3;
        if (cd == 0)
        {
            if (position.X < node.Position.X)
                node.Left = InsertRec(node.Left, position, depth + 1);
            else
                node.Right = InsertRec(node.Right, position, depth + 1);
        }
        else if (cd == 1)
        {
            if (position.Y < node.Position.Y)
                node.Left = InsertRec(node.Left, position, depth + 1);
            else
                node.Right = InsertRec(node.Right, position, depth + 1);
        }
        else // cd == 2
        {
            if (position.Z < node.Position.Z)
                node.Left = InsertRec(node.Left, position, depth + 1);
            else
                node.Right = InsertRec(node.Right, position, depth + 1);
        }

        return node;
    }

    public bool Search(Pos3D position)
    {
        return SearchRec(root, position, 0);
    }

    private bool SearchRec(Node node, Pos3D position, int depth)
    {
        if (node == null)
            return false;

        if (node.Position.X == position.X && node.Position.Y == position.Y && node.Position.Z == position.Z)
            return true;

        int cd = depth % 3;
        if (cd == 0)
        {
            if (position.X < node.Position.X)
                return SearchRec(node.Left, position, depth + 1);
            else
                return SearchRec(node.Right, position, depth + 1);
        }
        else if (cd == 1)
        {
            if (position.Y < node.Position.Y)
                return SearchRec(node.Left, position, depth + 1);
            else
                return SearchRec(node.Right, position, depth + 1);
        }
        else // cd == 2
        {
            if (position.Z < node.Position.Z)
                return SearchRec(node.Left, position, depth + 1);
            else
                return SearchRec(node.Right, position, depth + 1);
        }
    }
}
    
    public class MapManager3D
    {
        public int MinX { get; private set; }
        public int MaxX { get; private set; }
        public int MinY { get; private set; }
        public int MaxY { get; private set; }
        public int MinZ { get; private set; }
        public int MaxZ { get; private set; }

        private KdTree _collisionTree; 
        
        public KdTree CollisonTree
        {
            get { return _collisionTree; }
            private set { _collisionTree = value; }
        }

        public bool CanGo(Vector3Int cellPos)
        {
            // Check if the position is within the bounds
            if (cellPos.x < MinX || cellPos.x > MaxX || cellPos.y < MinY || cellPos.y > MaxY || cellPos.z < MinZ || cellPos.z > MaxZ)
                return false;

            // Check for collision using the k-d tree
            return !_collisionTree.Search(new KdTree.Pos3D(cellPos.x, cellPos.y, cellPos.z));
        }
        
        public void DestroyMap()
        {
            _collisionTree = new KdTree();
        }

         public void LoadMap(string filePath)
        {
            DestroyMap();

            if (!File.Exists(filePath))
            {
                Console.WriteLine($"Map file not found: {filePath}");
                return;
            }

            string[] lines = File.ReadAllLines(filePath);
            int lineIndex = 0;

            var gridWidth = int.Parse(lines[lineIndex++]);
            var gridHeight = int.Parse(lines[lineIndex++]);
            var gridDepth = int.Parse(lines[lineIndex++]);

            MinX = -gridWidth / 2;
            MaxX = gridWidth / 2 - 1;
            MinY = 0;
            MaxY = gridHeight - 1;
            MinZ = -gridDepth / 2;
            MaxZ = gridDepth / 2 - 1;

            _collisionTree = new KdTree();

            try
            {
                for (int y = MinY; y <= MaxY; y++)
                {
                    for (int z = MinZ; z <= MaxZ; z++)
                    {
                        string line = lines[lineIndex++];
                        if (line == null || line.Length < gridWidth)
                        {
                            Console.WriteLine("Unexpected end of map data or line length is less than expected grid width.");
                            return;
                        }

                        for (int x = MinX; x <= MaxX; x++)
                        {
                            int index = x + gridWidth / 2;
                            if (index < 0 || index >= line.Length)
                            {
                                Console.WriteLine($"Index {index} out of bounds for line: '{line}'.");
                                return;
                            }

                            if (line[index] == '1')
                            {
                                _collisionTree.Insert(new KdTree.Pos3D(x, y, z));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading map: {ex.Message}");
            }
        }
        
        
        #region A* PathFinding for 3D

        // Directions in 3D space: U, D, N, S, E, W, Edge Diagonals, and Vertex Diagonals
        private static readonly Vector3Int[] Directions = {
            new Vector3Int(1, 0, 0), new Vector3Int(-1, 0, 0), // East, West
            new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0), // Up, Down
            new Vector3Int(0, 0, 1), new Vector3Int(0, 0, -1), // North, South
            // Edge Diagonals
            new Vector3Int(1, 0, 1), new Vector3Int(-1, 0, 1), // EastNorth, WestNorth
            new Vector3Int(1, 0, -1), new Vector3Int(-1, 0, -1), // EastSouth, WestSouth
            new Vector3Int(1, 1, 0), new Vector3Int(-1, 1, 0), // EastUp, WestUp
            new Vector3Int(1, -1, 0), new Vector3Int(-1, -1, 0), // EastDown, WestDown
            new Vector3Int(0, 1, 1), new Vector3Int(0, -1, 1), // UpNorth, DownNorth
            new Vector3Int(0, 1, -1), new Vector3Int(0, -1, -1), // UpSouth, DownSouth
            // Vertex Diagonals
            new Vector3Int(1, 1, 1), new Vector3Int(-1, 1, 1), // UpNorthEast, UpNorthWest
            new Vector3Int(1, -1, 1), new Vector3Int(-1, -1, 1), // DownNorthEast, DownNorthWest
            new Vector3Int(1, 1, -1), new Vector3Int(-1, 1, -1), // UpSouthEast, UpSouthWest
            new Vector3Int(1, -1, -1), new Vector3Int(-1, -1, -1) // DownSouthEast, DownSouthWest
        };

    public List<Vector3Int> FindPath(Vector3Int startCellPos, Vector3Int destCellPos, bool ignoreDestCollision = false)
    {
        var closed = new HashSet<Vector3Int>();
        var openSet = new PriorityQueue<Vector3Int, int>();
        var gScore = new Dictionary<Vector3Int, int>();
        var fScore = new Dictionary<Vector3Int, int>();
        var cameFrom = new Dictionary<Vector3Int, Vector3Int>();

        gScore[startCellPos] = 0;
        fScore[startCellPos] = HeuristicCostEstimate(startCellPos, destCellPos);
        openSet.Enqueue(startCellPos, fScore[startCellPos]);

        while (openSet.Count > 0)
        {
            var current = openSet.Dequeue();
            if (current == destCellPos)
            {
                return ReconstructPath(cameFrom, current);
            }

            closed.Add(current);

            foreach (var direction in Directions)
            {
                var neighbor = current + direction;

                if (!CanGo(neighbor) || closed.Contains(neighbor))
                    continue;

                // Calculate cost based on the type of movement
                double movementCost = 1; // Default cost for orthogonal moves
                if (Math.Abs(direction.x) + Math.Abs(direction.y) + Math.Abs(direction.z) == 2)
                    movementCost = 1.41; // Edge diagonal move
                else if (Math.Abs(direction.x) + Math.Abs(direction.y) + Math.Abs(direction.z) == 3)
                    movementCost = 1.73; // Vertex diagonal move

                int tentativeGScore = gScore[current] + movementCost; // Update cost calculation

                if (!gScore.TryGetValue(neighbor, out int neighborGScore) || tentativeGScore < neighborGScore)
                {
                    cameFrom[neighbor] = current;
                    gScore[neighbor] = tentativeGScore;
                    fScore[neighbor] = tentativeGScore + HeuristicCostEstimate(neighbor, destCellPos);

                    if (!openSet.UnorderedItems.Any(item => item.Element.Equals(neighbor)))
                    {
                        openSet.Enqueue(neighbor, fScore[neighbor]);
                    }
                }
            }
        }

        return new List<Vector3Int>(); // Path not found
    }

    private List<Vector3Int> ReconstructPath(Dictionary<Vector3Int, Vector3Int> cameFrom, Vector3Int current)
    {
        List<Vector3Int> totalPath = new List<Vector3Int> { current };
        while (cameFrom.ContainsKey(current))
        {
            current = cameFrom[current];
            totalPath.Add(current);
        }
        totalPath.Reverse();
        return totalPath;
    }

    private int HeuristicCostEstimate(Vector3Int a, Vector3Int b)
    {
        // Calculate differences in each dimension
        int dx = Math.Abs(a.x - b.x);
        int dy = Math.Abs(a.y - b.y);
        int dz = Math.Abs(a.z - b.z);

        // Chebyshev distance for edge diagonals
        int chebyshev = Math.Max(dx, Math.Max(dy, dz));

        // Additional calculation for vertex diagonals using Euclidean distance
        int euclidean = (int)Math.Sqrt(dx * dx + dy * dy + dz * dz);

        return Math.Max(chebyshev, euclidean);
    }

    #endregion
    }
    
    public struct Vector3Int
    {
        public int x, y, z;

        public Vector3Int(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3Int operator +(Vector3Int a, Vector3Int b)
        {
            return new Vector3Int(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static bool operator ==(Vector3Int left, Vector3Int right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Vector3Int left, Vector3Int right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector3Int other)
            {
                return x == other.x && y == other.y && z == other.z;
            }
            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + x.GetHashCode();
                hash = hash * 23 + y.GetHashCode();
                hash = hash * 23 + z.GetHashCode();
                return hash;
            }
        }
    }
}