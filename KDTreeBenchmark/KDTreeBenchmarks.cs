namespace KDTreeBenchmark;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

[MemoryDiagnoser]
public class KdTreeBenchmarks
{
    private KdTree _kdTree;
    private MapManager3D _mapManager;
    private string _mapFilePath = "Map_001.txt"; // Ensure this path is correct

    [GlobalSetup]
    public void Setup()
    {
        _mapManager = new MapManager3D();
        _mapManager.LoadMap(_mapFilePath);
        _kdTree = _mapManager.CollisonTree;
    }

    [Benchmark]
    public void InsertionBenchmark()
    {
        _kdTree.Insert(new KdTree.Pos3D(10, 10, 10));
    }

    [Benchmark]
    public void SearchBenchmark()
    {
        _kdTree.Search(new KdTree.Pos3D(5, 5, 5));
    }

    [Benchmark]
    public void SearchStressTest()
    {
        for (int i = 0; i < 100000; i++)
        {
            _kdTree.Search(new KdTree.Pos3D(i % 10, i % 10, i % 10));
        }
    }
    
    [Benchmark]
    public void InsertionStressTest()
    {
        for (int i = 0; i < 300000; i++)
        {
            _kdTree.Insert(new KdTree.Pos3D(i % 10, i % 10, i % 10));
        }
    }
    
    [IterationSetup(Targets = new[] { nameof(SearchStressTest), nameof(InsertionStressTest) })]
    public void IterationSetupForStressTests()
    {
        _mapManager.LoadMap(_mapFilePath);
        _kdTree = _mapManager.CollisonTree;
    }
}