```

BenchmarkDotNet v0.13.10, macOS Sonoma 14.0 (23A344) [Darwin 23.0.0]
Intel Core i7-9750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK 8.0.100
  [Host]     : .NET 8.0.0 (8.0.23.53103), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.0 (8.0.23.53103), X64 RyuJIT AVX2
  Job-UQKIBN : .NET 8.0.0 (8.0.23.53103), X64 RyuJIT AVX2


```
| Method              | Job        | InvocationCount | UnrollFactor | Mean                 | Error               | StdDev              | Gen0      | Gen1      | Gen2      | Allocated  |
|-------------------- |----------- |---------------- |------------- |---------------------:|--------------------:|--------------------:|----------:|----------:|----------:|-----------:|
| InsertionBenchmark  | DefaultJob | Default         | 16           |      11,359,784.8 ns |     1,194,941.65 ns |     3,523,312.40 ns |         - |         - |         - |       48 B |
| SearchBenchmark     | DefaultJob | Default         | 16           |             176.7 ns |             0.37 ns |             0.33 ns |         - |         - |         - |          - |
| SearchStressTest    | Job-UQKIBN | 1               | 1            |      19,875,672.4 ns |       264,711.69 ns |       234,660.01 ns |         - |         - |         - |      736 B |
| InsertionStressTest | Job-UQKIBN | 1               | 1            | 133,946,455,677.6 ns | 1,088,495,633.95 ns | 1,018,179,491.35 ns | 2000.0000 | 1000.0000 | 1000.0000 | 14401408 B |
