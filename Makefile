# Path to the Unity editor executable - replace with your actual Unity path
UNITY_PATH := /Applications/Unity/Hub/Editor/2021.2.3f1/Unity.app/Contents/MacOS/Unity

# Project directories
UNITY_DIR := Grid3D
DOTNET_DIR := KDTreeBenchmark

# Default target executed when no arguments are given to make.
default: build

.PHONY: build test benchmark unity-test unity-build dotnet-build dotnet-test dotnet-benchmark

build: unity-build dotnet-build

test: unity-test dotnet-test

benchmark: dotnet-benchmark

unity-build:
	$(UNITY_PATH) -batchmode -nographics -silent-crashes -quit -projectPath $(UNITY_DIR) -executeMethod BuildScript.BuildAll

unity-test:
	$(UNITY_PATH) -batchmode -nographics -silent-crashes -quit -projectPath $(UNITY_DIR) -runTests -testResults $(UNITY_DIR)/TestResults.xml

dotnet-build:
	cd $(DOTNET_DIR) && dotnet build

dotnet-test:
	cd $(DOTNET_DIR) && dotnet test

dotnet-benchmark:
	cd $(DOTNET_DIR) && dotnet run -c Release --project KDTreeBenchmark.csproj -- --filter * --join