using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using UnityEngine;
using Utility;

namespace Editor
{
    [CustomEditor(typeof(GridData3D))]
    public class GridEditor3D : UnityEditor.Editor
    {
        GridData3D gridData;
        bool paintWalkable = false;

        void OnEnable()
        {
            gridData = (GridData3D)target;
            SceneView.duringSceneGui += OnSceneGUI;
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        void OnSceneGUI(SceneView sceneView)
        {
            Handles.BeginGUI();
            GUILayout.BeginArea(new Rect(10, 20, 250, 250));

            if (GUILayout.Button("Layer Up"))
            {
                gridData.currentLayer += 1;
                SceneView.RepaintAll();
            }

            if (GUILayout.Button("Layer Down"))
            {
                gridData.currentLayer -= 1;
                SceneView.RepaintAll();
            }

            if (GUILayout.Button("Toggle Paint Mode"))
            {
                paintWalkable = !paintWalkable;
            }

            if (GUILayout.Button("Make Walls Unwalkable"))
            {
                gridData.MakeWallsUnwalkable();
                EditorUtility.SetDirty(gridData);
                SceneView.RepaintAll();
            }
            
            if (GUILayout.Button("Update Walkability From Colliders"))
            {
                gridData.StartWalkabilityUpdate();
                EditorUtility.SetDirty(gridData);
                SceneView.RepaintAll();
            }

            if (GUILayout.Button("Save Grid"))
            {
                string path = EditorUtility.SaveFilePanel("Save Grid", "", "GridData", "txt");
                if (!string.IsNullOrEmpty(path))
                {
                    gridData.SaveGrid(path);
                }

                GUIUtility.ExitGUI();
            }

            if (GUILayout.Button("Load Grid"))
            {
                string path = EditorUtility.OpenFilePanel("Load Grid", "", "txt");
                if (!string.IsNullOrEmpty(path))
                {
                    gridData.LoadGrid(path);
                }

                GUIUtility.ExitGUI();
            }

            GUILayout.Label("Painting: " + (paintWalkable ? "Walkable" : "Unwalkable"));
            GUILayout.Label($"Grid Size: {gridData.gridWidth} x {gridData.gridHeight} x {gridData.gridDepth}");

            gridData.gridWidth = EditorGUILayout.IntField("Width", gridData.gridWidth);
            gridData.gridHeight = EditorGUILayout.IntField("Height", gridData.gridHeight);
            gridData.gridDepth = EditorGUILayout.IntField("Depth", gridData.gridDepth);

            if (GUILayout.Button("Update Grid Size"))
            {
                gridData.UpdateGridSize();
            }

            GUILayout.EndArea();
            Handles.EndGUI();

            // HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            Event e = Event.current;

            if ((e.type == EventType.MouseDown || e.type == EventType.MouseDrag) && e.button == 0)
            {
                if (!e.alt && !e.shift && !e.control && GUIUtility.hotControl == 0)
                {
                    Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                    if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
                    {
                        // Translate the hit point into local space relative to the grid.
                        Vector3 localHitPoint = gridData.transform.InverseTransformPoint(hit.point);

                        // Adjust for the origin being at the center of the grid
                        int gridX = Mathf.FloorToInt((localHitPoint.x / gridData.nodeSize) + (gridData.gridWidth / 2));
                        int gridZ = Mathf.FloorToInt((localHitPoint.z / gridData.nodeSize) + (gridData.gridDepth / 2));

                        // Ensure we're within the bounds of the grid
                        if (gridX >= 0 && gridX < gridData.gridWidth && gridZ >= 0 && gridZ < gridData.gridDepth)
                        {
                            // Paint on the current layer only
                            gridData.SetWalkability(gridX - (gridData.gridWidth / 2), gridData.currentLayer, gridZ - (gridData.gridDepth / 2), paintWalkable);
                            e.Use();
                            SceneView.RepaintAll();
                        }
                    }
                }
            }

            if (e.type == EventType.Layout)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            }
        }
    }
}
