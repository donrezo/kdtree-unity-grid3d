using System.Linq;
using Managers;
using NUnit.Framework;
using UnityEngine;

namespace EditMode
{
    [TestFixture]
    public class MapManager3DTests
    {
        private MapManager3D _mapManager;

        [SetUp]
        public void Setup()
        {
            _mapManager = new MapManager3D();
            _mapManager.LoadMap(1);
        }

        [TearDown]
        public void Teardown()
        {
            // Cleanup
            _mapManager.DestroyMap();
            _mapManager = null;
        }

        [Test]
        public void MapManager3D_LoadMap_CreatesCorrectGridStructure()
        {
            // Check that the grid has been loaded with the correct dimensions
            Assert.AreEqual(100, _mapManager.MaxX - _mapManager.MinX + 1);
            Assert.AreEqual(10, _mapManager.MaxY - _mapManager.MinY + 1);
            Assert.AreEqual(20, _mapManager.MaxZ - _mapManager.MinZ + 1);
        }

        [Test]
        public void MapManager3D_CanGo_ReturnsFalseForEdge()
        {
            // Check unwalkable edge
            Assert.IsFalse(_mapManager.CanGo(new Vector3Int(_mapManager.MinX, 0, _mapManager.MinZ)));
            Assert.IsFalse(_mapManager.CanGo(new Vector3Int(_mapManager.MaxX, _mapManager.MaxY, _mapManager.MaxZ)));
        }

        [Test]
        public void MapManager3D_CanGo_ReturnsTrueForInside()
        {
            // Check walkable inside area
            Assert.IsTrue(_mapManager.CanGo(new Vector3Int(_mapManager.MinX + 1, 0, _mapManager.MinZ + 1)));
            Assert.IsTrue(
                _mapManager.CanGo(new Vector3Int(_mapManager.MaxX - 1, _mapManager.MaxY, _mapManager.MaxZ - 1)));
        }
        
         [Test]
        public void MapManager3D_FindPath_IncludesDiagonalMovement()
        {
            // Test that the path includes diagonal movement
            Vector3Int start = new Vector3Int(0, 0, 0);
            Vector3Int end = new Vector3Int(2, 2, 2); // A point that would require diagonal movement
            var path = _mapManager.FindPath(start, end);
            
            // Ensure the path is not empty and includes expected diagonal steps
            Assert.IsNotEmpty(path);
            Assert.Contains(new Vector3Int(1, 1, 1), path); // A point on a diagonal path
        }

        [Test]
        public void MapManager3D_FindPath_AvoidsObstaclesWithDiagonals()
        {
            // Test that the pathfinding correctly avoids obstacles even when considering diagonals
            Vector3Int start = new Vector3Int(0, 0, 0);
            Vector3Int obstacle = new Vector3Int(1, 1, 1); // An obstacle in a direct diagonal path
            Vector3Int end = new Vector3Int(2, 2, 2);

            // Insert an obstacle
            _mapManager.CollisonTree.Insert(new KdTree.Pos3D(obstacle.x, obstacle.y, obstacle.z));

            var path = _mapManager.FindPath(start, end);

            // Ensure the path avoids the obstacle
            Assert.IsNotEmpty(path);
            Assert.IsFalse(path.Contains(obstacle)); // Path should not contain the obstacle
        }

        [Test]
        public void MapManager3D_FindPath_CorrectLengthAndDirection()
        {
            // Test the path length and direction including diagonals
            Vector3Int start = new Vector3Int(0, 0, 0);
            Vector3Int end = new Vector3Int(3, 3, 3); // A point that would require multiple diagonal movements
            
            var path = _mapManager.FindPath(start, end);
            
            // Check if the path length is as expected
            int expectedLength = 4;
            Assert.AreEqual(expectedLength, path.Count);
            
            // Optionally, check the first and last elements in the path
            Assert.AreEqual(start, path.First());
            Assert.AreEqual(end, path.Last());
        }
    }
}