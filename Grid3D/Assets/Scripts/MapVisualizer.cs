using Managers;
using UnityEngine;

namespace DefaultNamespace
{
    public class MapVisualizer : MonoBehaviour
    {
        public MapManager3D mapManager;
        public int mapIdToLoad = 1;

        void Start()
        {
            mapManager = new MapManager3D();
            LoadAndVisualizeMap();
        }

        void LoadAndVisualizeMap()
        {
            mapManager.LoadMap(mapIdToLoad);
        }

        void OnDrawGizmos()
        {
            if (mapManager == null || mapManager.CollisonTree == null)
            {
                return;
            }

            // Call a method that starts the recursive drawing process
            DrawKdTree(mapManager.CollisonTree.Root, 0);
        }

        void DrawKdTree(KdTree.Node node, int depth)
        {
            if (node == null)
            {
                return;
            }

            // Determine color based on depth for visualization purposes
            Gizmos.color = depth % 3 == 0 ? Color.red : (depth % 3 == 1 ? Color.green : Color.blue);

            // Convert Pos3D to Vector3
            Vector3 position = new Vector3(node.Position.X, node.Position.Y, node.Position.Z);

            // Draw the node
            Gizmos.DrawCube(position, Vector3.one * 0.1f); // Adjust the size as needed

            // Continue for children
            DrawKdTree(node.Left, depth + 1);
            DrawKdTree(node.Right, depth + 1);
        }
    }
}