using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace CharacterController
{
    public class CharacterController : MonoBehaviour
    {
        public MapManager3D mapManager;
        public float moveSpeed = 5f;

        private Vector3Int targetPosition;
        private List<Vector3Int> path;
        private int pathIndex;
        private bool isManualMoving = false;
        public int maxFallDistance = 10;
        
        public void Start()
        {
            mapManager = new MapManager3D();
            mapManager.LoadMap(1);
        }

        void Update()
        {
            HandleManualMovement();

            if (Input.GetMouseButtonDown(0) && !isManualMoving)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    targetPosition = Vector3Int.RoundToInt(hit.point);
                    CalculatePath();
                }
            }

            if (!isManualMoving)
            {
                MoveAlongPath();
            }
            
            ApplyGravity();
        }
        
        void ApplyGravity()
        {
            // Only apply gravity if the character is not currently moving manually
            if (!isManualMoving)
            {
                Vector3Int currentPosition = Vector3Int.RoundToInt(transform.position);
                bool movedDown = false;

                // Find the nearest walkable surface below, up to the max fall distance
                for (int y = 1; y <= maxFallDistance; y++)
                {
                    Vector3Int below = new Vector3Int(currentPosition.x, currentPosition.y - y, currentPosition.z);
                    if (mapManager.CanGo(below))
                    {
                        // Move the character down to the first walkable position found
                        transform.position = new Vector3(transform.position.x, below.y, transform.position.z);
                        movedDown = true;
                        break;
                    }
                }

                if (!movedDown)
                {
                    // Handle the case where no walkable position was found below the character
                    // This could be a fall to an invalid area, game over condition, etc.
                }
            }
        }

        void CalculatePath()
        {
            Vector3Int currentPosition = Vector3Int.RoundToInt(transform.position);
            path = mapManager.FindPath(currentPosition, targetPosition);
            pathIndex = 0;
        }

        void MoveAlongPath()
        {
            if (path != null && path.Count > 0 && pathIndex < path.Count)
            {
                Vector3 target = path[pathIndex];
                transform.position = Vector3.MoveTowards(transform.position, target, moveSpeed * Time.deltaTime);

                if (Vector3.Distance(transform.position, target) < 0.1f)
                {
                    pathIndex++;
                }
            }
        }
        
        void HandleManualMovement()
        {
            Vector3 proposedMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * moveSpeed * Time.deltaTime;
            Vector3Int newPosition = Vector3Int.RoundToInt(transform.position + proposedMovement);

            if (proposedMovement != Vector3.zero && mapManager.CanGo(newPosition))
            {
                isManualMoving = true;
                transform.Translate(proposedMovement, Space.World);
            }
            else
            {
                isManualMoving = false;
            }
        }
        
        
        
        void OnDrawGizmos()
        {
            if (path != null && path.Count > 1)
            {
                Gizmos.color = Color.green;

                for (int i = 0; i < path.Count - 1; i++)
                {
                    Vector3 start = path[i];
                    Vector3 end = path[i + 1];
                
                    Gizmos.DrawLine(start, end);
                }
            }
        }
    }
}