using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Utility
{
    public class GridData3D : MonoBehaviour
    {
        public float nodeSize = 1.0f;
        private Dictionary<Vector3Int, bool> grid = new Dictionary<Vector3Int, bool>();
        public int gridWidth = 10; // default grid width
        public int gridHeight = 10; // default grid height
        public int gridDepth = 10; // default grid depth
        public int currentLayer = 0;

        public bool GetWalkability(int x, int y, int z)
        {
            Vector3Int key = new Vector3Int(x, y, z);
            if (grid.TryGetValue(key, out bool walkable))
            {
                return walkable;
            }

            // Default walkable state
            return true;
        }

        public void UpdateGridSize()
        {
            var newGrid = new Dictionary<Vector3Int, bool>();

            for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
            {
                for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                {
                    for (int y = 0; y < gridHeight; y++)
                    {
                        Vector3Int key = new Vector3Int(x, y, z);
                        bool walkable = grid.TryGetValue(key, out bool currentWalkable) ? currentWalkable : true;
                        newGrid[key] = walkable;
                    }
                }
            }

            grid = newGrid;
        }

        public void SetWalkability(int x, int y, int z, bool walkable)
        {
            Vector3Int key = new Vector3Int(x, y, z);
            if (grid.ContainsKey(key))
            {
                grid[key] = walkable;
            }
            else
            {
                grid.Add(key, walkable);
            }
        }

        void OnDrawGizmosSelected()
        {
            if (Application.isPlaying) return;

            // Get the scene view camera position to determine which part of the grid to draw
            Vector3 sceneCamPosition = SceneView.lastActiveSceneView.camera.transform.position;

            // Calculate the distance from the camera to the grid origin
            Vector3 toCam = sceneCamPosition - transform.position;
            int camX = Mathf.FloorToInt(toCam.x / nodeSize) + gridWidth / 2;
            int camZ = Mathf.FloorToInt(toCam.z / nodeSize) + gridDepth / 2;

            // Define the range of the grid to draw
            int drawRangeX = Mathf.Clamp(camX, 0, gridWidth - 1);
            int drawRangeZ = Mathf.Clamp(camZ, 0, gridDepth - 1);

            Vector3 gridOffset = new Vector3((gridWidth * nodeSize) / 2, 0, (gridDepth * nodeSize) / 2);
            Vector3 startPoint = transform.position - gridOffset;

            // Calculate the start and end points for drawing
            int startX = Mathf.Max(drawRangeX - 25, 0);
            int endX = Mathf.Min(drawRangeX + 25, gridWidth);
            int startZ = Mathf.Max(drawRangeZ - 25, 0);
            int endZ = Mathf.Min(drawRangeZ + 25, gridDepth);

            for (int x = startX; x < endX; x++)
            {
                for (int z = startZ; z < endZ; z++)
                {
                    // Only draw the current layer
                    Vector3 nodePoint = startPoint + new Vector3(x * nodeSize, currentLayer * nodeSize, z * nodeSize);
                    bool walkable = GetWalkability(x - gridWidth / 2, currentLayer, z - gridDepth / 2);
                    Gizmos.color = walkable ? Color.white : Color.red;
                    Gizmos.DrawCube(nodePoint, Vector3.one * (nodeSize - 0.1f));
                }
            }
        }

        public void MakeWallsUnwalkable()
        {
            for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
            {
                for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                {
                    for (int y = 0; y < gridHeight; y++)
                    {
                        // Set the perimeter nodes as unwalkable
                        if (x == -gridWidth / 2 || x == (gridWidth / 2) - 1 || z == -gridDepth / 2 ||
                            z == (gridDepth / 2) - 1)
                        {
                            SetWalkability(x, y, z, false);
                        }
                    }
                }
            }
        }

        public void SaveGrid(string filePath)
        {
            StringBuilder sb = new StringBuilder();

            // Write the dimensions at the top of the file
            sb.AppendLine(gridWidth.ToString());
            sb.AppendLine(gridHeight.ToString());
            sb.AppendLine(gridDepth.ToString());

            // Save the grid data
            for (int y = 0; y < gridHeight; y++)
            {
                for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                {
                    for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
                    {
                        bool walkable = GetWalkability(x, y, z);
                        sb.Append(walkable ? "0" : "1");
                    }

                    sb.AppendLine(); // New line for each layer slice
                }
            }

            File.WriteAllText(filePath, sb.ToString());
        }

        public void LoadGrid(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);

            // Basic validation
            if (lines.Length < 3) // Must have at least three lines for the dimensions
            {
                Debug.LogError("Grid data file is too short.");
                return;
            }

            // Parse dimensions from the first three lines
            gridWidth = int.Parse(lines[0]);
            gridHeight = int.Parse(lines[1]);
            gridDepth = int.Parse(lines[2]);

            // Ensure there are enough lines for the grid data
            if (lines.Length < 3 + gridHeight * (gridDepth / 2)) // Assuming each layer's data is on a separate line
            {
                Debug.LogError("Grid data file does not contain enough lines for expected grid dimensions.");
                return;
            }

            grid.Clear();
            int lineIndex = 3; // Start reading from the fourth line

            // Read the grid state line by line
            for (int y = 0; y < gridHeight; y++)
            {
                for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                {
                    string line = lines[lineIndex++];
                    for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
                    {
                        // Translate grid coordinates to index in line
                        int index = x + (gridWidth / 2);
                        if (index < 0 || index >= line.Length)
                        {
                            Debug.LogError("Index out of bounds when reading grid data.");
                            return;
                        }

                        bool walkable = line[index] == '0'; // Assumes '0' is walkable
                        grid[new Vector3Int(x, y, z)] = walkable;
                    }
                }
            }
        }

        public void StartWalkabilityUpdate()
        {
            StartCoroutine(UpdateWalkabilityFromCollidersInBatches());
        }

        private IEnumerator UpdateWalkabilityFromCollidersInBatches()
        {
            int batchSize = 400; // Number of nodes to process per frame before yielding
            int totalNodes = gridWidth * gridDepth * (gridHeight - 1); // Total nodes excluding ground level
            int processedNodes = 0;

            // Set ground level (y = 0) as unwalkable
            for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
            {
                for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                {
                    SetWalkability(x, 0, z, false);
                }
            }

            // Start coroutine to update walkability for layers above ground
            for (int y = 1; y < gridHeight; y++)
            {
                for (int x = -gridWidth / 2; x < gridWidth / 2; x++)
                {
                    for (int z = -gridDepth / 2; z < gridDepth / 2; z++)
                    {
                        Vector3 nodeCenter = transform.position + new Vector3(x * nodeSize, y * nodeSize - nodeSize * 0.5f, z * nodeSize);
                        bool walkable = !Physics.CheckBox(nodeCenter, new Vector3(nodeSize * 0.5f, nodeSize * 0.1f, nodeSize * 0.5f), Quaternion.identity);
                        SetWalkability(x, y, z, walkable);

                        processedNodes++;
                        if (processedNodes % batchSize == 0)
                        {
                            float progress = (float)processedNodes / totalNodes * 100;
                            Debug.Log($"Update Walkability Progress: {processedNodes}/{totalNodes} ({progress:F2}%)");
                            yield return null; // Yield execution to maintain responsiveness
                        }
                    }
                }
            }
            Debug.Log("Walkability update complete!");
        }
    }
}